package com.tsystems.javaschool.tasks.calculator;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class ExpressionChecker {
    Integer TotalNesting = 0;
    Boolean IsExprCorrect = true;

    ExpressionChecker(String Expression) {
        if (isExpressionEmpty(Expression)) {
            IsExprCorrect = false;
        } else {
            if (!isExpressionCorrect(Expression)) {
                IsExprCorrect = false;
            } else if (!isBracketsCorrect(Expression)) {
                IsExprCorrect = false;
            }
        }
    }

    private Boolean isBracketsCorrect(String Expression) {
        int LeftBracketsCount = 0;
        int RightBracketsCount = 0;
        Pattern PatternLeft = Pattern.compile("\\(");
        Matcher MatcherLeft = PatternLeft.matcher(Expression);
        Pattern PatternRight = Pattern.compile("\\)");
        Matcher MatcherRight = PatternRight.matcher(Expression);
        while (MatcherLeft.find()) LeftBracketsCount++;
        while (MatcherRight.find()) RightBracketsCount++;
        if (LeftBracketsCount == RightBracketsCount) TotalNesting = LeftBracketsCount;
        return LeftBracketsCount == RightBracketsCount;
    }

    private Boolean isExpressionCorrect(String Expression) {
        Pattern ExprPattern = Pattern.compile("(\\.|\\+|\\*|/|-){2,}");
        Matcher ExprMatcher = ExprPattern.matcher(Expression);
        return !ExprMatcher.find();
    }

    private Boolean isExpressionEmpty(String Expression) {
        if ((Objects.equals(Expression, ""))||(Objects.equals(Expression, null))) {
            return true;
        }
            return false;
    }
}
