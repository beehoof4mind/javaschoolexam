package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    private static String getInnerExpression(String statement) {
        Pattern ExprPattern = Pattern.compile("\\((.+)\\)");
        Matcher ExprMatcher = ExprPattern.matcher(statement);
        ExprMatcher.find();
        return ExprMatcher.group(1);
    }

    String evaluate(String statement) {
        ExpressionChecker expressionChecker = new ExpressionChecker(statement);
        ExpressionCalc expressionCalc = new ExpressionCalc();
        if (expressionChecker.IsExprCorrect) {
            if (expressionChecker.TotalNesting > 1) {
                String FinalInnerValue = null;
                ArrayList<String> arrayList = new ArrayList<>();
                for (int i = 0; i < expressionChecker.TotalNesting; i++) {
                    arrayList.add(getInnerExpression(statement));
                    statement = getInnerExpression(statement);
                }

                for (int i = arrayList.size() - 1; i >= 0; i--) {
                    if (i > 0) {
                        String CurrentString = arrayList.get(i);
                        String CurrentValue = expressionCalc.CalculateValue(CurrentString);
                        String PreviousValue = arrayList.get(i - 1);
                        PreviousValue = PreviousValue.replace("(" + CurrentString + ")", CurrentValue);
                        arrayList.set(i - 1, PreviousValue);
                    } else {
                        String CurrentString = arrayList.get(i);
                        String CurrentValue = expressionCalc.CalculateValue(arrayList.get(i + 1));
                        FinalInnerValue = expressionCalc.CalculateValue(CurrentString.replaceAll("\\(.+\\)", CurrentValue));
                    }
                }
                return expressionCalc.CalculateValue(statement.replaceAll("\\(.+\\)", FinalInnerValue));

            } else if (expressionChecker.TotalNesting == 1) {
                String InnerExpression = getInnerExpression(statement);
                String CalculatedValue = expressionCalc.CalculateValue(InnerExpression);
                if (Integer.parseInt(CalculatedValue)<0) {
                    CalculatedValue = CalculatedValue.replaceAll("-", "");
                    String CurrentValue = statement.replaceAll("\\(.+\\)", CalculatedValue);
                    return  "-" + expressionCalc.CalculateValue(CurrentValue);
                } else {
                    String CurrentValue = statement.replaceAll("\\(.+\\)", CalculatedValue);
                    return expressionCalc.CalculateValue(CurrentValue);
                }

            } else return expressionCalc.CalculateValue(statement);
        } else return null;

    }
}



