package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class ArgumentChecker {
    Boolean ISARGSVALID;

    public ArgumentChecker(List x, List y) {
        this.ISARGSVALID = checkArgumentsValid(x, y);
    }

    Boolean checkArgumentsValid(List x, List y) {
        if ((x == null)||(y == null)) {
            return false;
        } else return true;
    }
}
