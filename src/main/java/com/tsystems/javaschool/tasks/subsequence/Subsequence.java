package com.tsystems.javaschool.tasks.subsequence;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException {
        ArgumentChecker argumentChecker = new ArgumentChecker(x, y);
        if (!argumentChecker.ISARGSVALID) throw new IllegalArgumentException();
        if (x.size() == 0) {
            return true;
        } else if (y.size()==0) {
            return false;
        } else {
            ArrayList<BigInteger> HashXList = new ArrayList<>();
            ArrayList<BigInteger> HashYList = new ArrayList<>();
            for (Object aX : x) {
                HashXList.add(BigInteger.valueOf(aX.hashCode()));
            }
            for (Object aY : y) {
                HashYList.add(BigInteger.valueOf(aY.hashCode()));
            }
            Integer CurrentIteration = 0;
            Integer CountPassed = 0;
            for (BigInteger aHashXList : HashXList) {
                for (int j = CurrentIteration; j < HashYList.size(); ) {
                    if (!Objects.equals(aHashXList, HashYList.get(j))) {
                        j++;
                    } else {
                        CountPassed++;
                        CurrentIteration = j + 1;
                        break;
                    }
                }
            }
            return CountPassed.equals(x.size());
        }
    }
}
