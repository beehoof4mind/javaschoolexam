package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        ListValidator listValidator = new ListValidator(inputNumbers.size());
        if ((!listValidator.CanBuild)||(inputNumbers.contains(null))) throw new CannotBuildPyramidException();
        Collections.sort(inputNumbers);
        Integer Lines = listValidator.TotalLines;
        Integer Rows = Lines*2-1;
        Integer CentralIndex = Rows/2;
        Integer InlineElements = 1;
        Integer InlineIndex = 0;
        Integer DisPlacement = 0;
        int[][] PyramidMatrix = new int[Lines][Rows];

        for(int i = 0; i < Lines; i++){
            int Position = CentralIndex-DisPlacement;
            for(int j = 0; j < InlineElements*2; j+=2){
                PyramidMatrix[i][Position+j] = inputNumbers.get(InlineIndex);
                InlineIndex++;
            }
            DisPlacement++;
            InlineElements++;
        }
        return PyramidMatrix;
    }
}
