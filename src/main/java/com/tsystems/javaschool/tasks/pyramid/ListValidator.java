package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Arrays;

class ListValidator {
    Boolean CanBuild = false;
    Integer TotalLines;

    ListValidator(Integer ListSize) {
        this.CanBuild = canBuildPyramid(ListSize);
    }

    private Boolean canBuildPyramid(Integer ListSize) throws CannotBuildPyramidException{
        ArrayList<Integer> CorrectValuesList = new ArrayList<>();
        CorrectValuesList.addAll(Arrays.asList(0, 1));
        if (ListSize >= Integer.MAX_VALUE - 1) throw new CannotBuildPyramidException();
        Integer RoundedMaxIterations = Math.toIntExact(Math.round(ListSize + ListSize * 0.1));
        for (int i = 1; i < RoundedMaxIterations; i++) {
            if (ListSize.equals(CorrectValuesList.get(i))) {
                this.TotalLines = i;
                return true;
            } else {
                CorrectValuesList.add(CorrectValuesList.get(i) + i + 1);
            }
        }
        return false;

    }
}
